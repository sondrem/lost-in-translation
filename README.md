# Sign Language Translator using React

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

An online sign language translator build as a Single Page Application using the React framework. 

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Integration](#Integration)
- [Built with](#built-with)
- [Contributing](#contributing)
- [License](#license)

## About
This project is about building an online sign language translator as a Single Page Application using the React framework. The application communicates with the server through the REST API found here: https://sm-lost-in-translation-api.herokuapp.com/ to store users and translations. 

### Appendix A: Requirements for the sign language translator
The application has one main feature: to act as a “translator” from regular text to sign language. The application translates English words and short sentences to American sign language. The images for the sign language
will are included in the public folder. Please note that special characters are ignored and leading and trailing whitespaces. The application contains three main screens: 
#### Startup page
The first thing a user  sees is the “Login page” where the user has to enter their name. The username is saved to the Translation API. 
Users that are already logged in is automatically be redirected to the Translation page. This is managed by the browsers local storage. 

#### Translation page
A user can only view this page if they are currently logged into the app. The user is  redirected back to the login page if
now active login session exists in the browser storage.
The user types in the input box at the top of the page. The user must click on the “translate” button to the right of the input box to trigger the translation. The Sign language characters will appear below in the “translated” box. There is no limit for number of characters. 
Only the english alphabet is used. Special characters and leading and 
trailing spaces are ignored.
#### Profile page
The profile page displays the text from the last 10 translations for the current user. There is also a button to clear the translations. 
The Logout button clears all the storage and returns to the start page. 

### Component tree: With figma
The pdf file called "component tree" is a component tree of the application. The component tree shows the pages and feature components we planned to create in our application. Changes was done under development in a modern agile 
style. The final component tree can be seen on request. 

## Install
To run the application locally: clone repository build project.  
Please note an api-key is needed to connect to the server. 

## Usage
Run locally with npm start.
Also, you can visit the deployed application: 
https://sm-lost-in-translation-app.herokuapp.com/ 

## Integration
The application is hosted on heroku: please follow this link. 
https://sm-lost-in-translation-app.herokuapp.com/ 

## Built with

- Figma
- NPM/Node.js (LTS – Long Term Support version)
- React CRA (create-react-app)
- React Bootstrap
- JavaScript
- Visual Studio Code Text Editor
- Browser Developer Tools for testing and debugging
- React Dev Tools
- Git
- Rest API: https://sm-lost-in-translation-api.herokuapp.com/
- Heroku

## Contributing
- [Lars-Inge Gammelsæter Jonsen](https://gitlab.com/Kaladinge)
- [Sondre Mæhre](https://gitlab.com/sondrem)

PRs accepted.

## License

UNLICENSED