import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

/**
 * Method to add translation to users history
 * @param {*} user User to add the translation record to 
 * @param {*} translation Translation record to be added
 * @returns response ok or not
 */
export const translationAdd = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation],
            }),
        });

        if (!response.ok) {
            throw new Error("Could not update the translation.");
        }
        const result = await response.json();
        return [null, result];
    } catch (error) {
        return [error.message, null];
    }
};

/**
 * Method to delete users history
 * @param {*} userId id of the user to clear the history from
 * @returns response ok or not
 */
export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [],
            }),
        });
        if (!response.ok) {
            throw new Error("Could not update the translation.");
        }
        const result = await response.json();
        return [null, result];
    } catch (error) {
        return [error.message, null];
    }
};
