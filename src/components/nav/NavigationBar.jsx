import { useUser } from "../../context/UserContext";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

const NavigationBar = () => {
    const { user } = useUser();

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="/">Lost in Translations</Navbar.Brand>
                {user !== null && (
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                )}
                {user !== null && (
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/translator">Translator</Nav.Link>
                            <Nav.Link href="/profile">Profile</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                )}
            </Container>
        </Navbar>
    );
};

export default NavigationBar;
