import { translationClearHistory } from "../../api/translations";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";
import Button from "react-bootstrap/Button";

const ProfileActions = ({ logout }) => {
    const { user, setUser } = useUser();

    //event handlers
    const handleLogoutClick = async () => {
        const [clearError] = await translationClearHistory(user.id);

        if (clearError !== null) {
            return;
        }

        if (window.confirm("Are you sure?")) {
            logout();
        }
    };

    const handleClearHistoryClick = async () => {
        if (!window.confirm("Are you sure?\nThis can not be undone!")) {
            return;
        }

        const [clearError] = await translationClearHistory(user.id);

        if (clearError !== null) {
            return;
        }

        const updatedUser = {
            ...user,
            translations: [],
        };

        storageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
    };

    return (
        <div className="d-grid gap-2">
            <Button
                size="xs"
                variant="primary"
                onClick={handleClearHistoryClick}
            >
                Clear translation history
            </Button>
            <Button variant="primary" onClick={handleLogoutClick}>
                Logout
            </Button>
        </div>
    );
};

export default ProfileActions;
