const ProfileHeader = ({ username }) => {
    return (
        <header>
            <h3> Hello, welcome back {username} </h3>
            <h4> Watch your last 10 translations below </h4>
        </header>
    );
};

export default ProfileHeader;
