/**
 * 
 * @param {*} item translation to be added to list
 * @returns part of component
 */
const ProfileTranslationHistoryItem = ({ item }) => {
    return <li className="historyItem">{item}</li>;
};

export default ProfileTranslationHistoryItem;
