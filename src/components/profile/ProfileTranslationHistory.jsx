import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

/**
 * Shows ten latest translations
 * @param {array} translations - translation array
 */
const ProfileTranslationHistory = ({ translations }) => {
    const translationList = translations
        .slice(0, 10)
        .map((translation, index) => (
            <ProfileTranslationHistoryItem
                key={index + "-" + translation}
                item={translation}
            />
        ));
    return (
        <section>
            <h4>Your last translations</h4>
            {translationList.length === 0 && (
                <p>You have no translations yet.</p>
            )}
            <ul>{translationList}</ul>
        </section>
    );
};

export default ProfileTranslationHistory;
