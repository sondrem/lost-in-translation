/**
 * @param {*} sign sign language symbol to be showed
 * @returns the image to be displayed
 */
const TranslatorOutputSign = ({ sign }) => {
    return <img src={sign.image} alt="Sign icon" />;
};

export default TranslatorOutputSign;
