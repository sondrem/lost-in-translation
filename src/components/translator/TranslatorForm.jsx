import { useForm } from "react-hook-form";
import Button from "react-bootstrap/Button";
import { Form, InputGroup } from "react-bootstrap";

const TranslatorForm = ({ onTranslate }) => {
    const { register, handleSubmit } = useForm();

    const onSubmit = ({ translatorInput }) => {
        onTranslate(translatorInput);
    };

    return (
        <Form onSubmit={handleSubmit(onSubmit)} className="customForm">
            <fieldset>
                <label htmlFor="username"> </label>
                <InputGroup className="mb-2">
                    <InputGroup.Text>
                        <img
                            src="/images/keyboard.png"
                            alt="keyboard icon"
                            width="30px"
                        />
                    </InputGroup.Text>
                    <Form.Control
                        id="inlineFormInputGroup"
                        placeholder="Insert text to translate"
                        {...register("translatorInput")}
                    />
                    <InputGroup.Text>
                        <Button variant="primary" type="submit">
                            <img
                                src="/images/arrow.png"
                                alt="arrow icon"
                                width="30px"
                            />
                        </Button>
                    </InputGroup.Text>
                </InputGroup>
            </fieldset>
        </Form>
    );
};

export default TranslatorForm;
