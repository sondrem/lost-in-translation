import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useState, useEffect } from "react";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import Button from "react-bootstrap/Button";
import { Col, Row } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";

const usernameConfig = {
    required: true,
    minLength: 6,
};

const LoginForm = () => {
    //hooks
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    //local state
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    //side effects
    useEffect(() => {
        console.log("hi");
        if (user !== null) {
            navigate("/translator");
        }
        console.log("User has changed", user);
    }, [user, navigate]);

    //Event handlers
    /**
     * Logs in user
     * @param {string} username - Username
     */
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);
        if (error !== null) {
            setApiError(error);
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse);
            setUser(userResponse);
        }
        setLoading(false);
    };

    //render function
    const errorMessage = (() => {
        if (!errors.username) return null;

        if (errors.username.type === "required")
            return <span>Username is required</span>;

        if (errors.username.type === "minLength")
            return <span>Username has to be more than 5 letters</span>;
    })();

    return (
        <>
            <Row className="justify-content-md-center">
                <Col xs lg="3">
                    <img
                        src="images/robot.png"
                        alt="Logo of robot"
                        width="100%"
                    />
                </Col>
                <Col
                    xs
                    lg="5"
                    className="d-flex flex-column justify-content-md-center"
                >
                    <h2>Welcome to Lost in Translation</h2>
                    <h3>Get started</h3>
                </Col>
            </Row>

            <Form onSubmit={handleSubmit(onSubmit)} className="customForm">
                <fieldset>
                    <label htmlFor="username"> </label>
                    <InputGroup className="mb-2">
                        <InputGroup.Text>
                            <img
                                src="/images/keyboard.png"
                                alt="keyboard icon"
                                width="30px"
                            />
                        </InputGroup.Text>
                        <Form.Control
                            id="inlineFormInputGroup"
                            placeholder="Username"
                            {...register("username", usernameConfig)}
                        />
                        <InputGroup.Text>
                            <Button
                                variant="primary"
                                type="submit"
                                disabled={loading}
                            >
                                <img
                                    src="/images/arrow.png"
                                    alt="arrow icon"
                                    width="30px"
                                />
                            </Button>
                        </InputGroup.Text>
                    </InputGroup>
                    {errorMessage}
                </fieldset>
                {loading && <p>Logging in...</p>}
                {apiError && <p> {apiError}</p>}
            </Form>
        </>
    );
};

export default LoginForm;
