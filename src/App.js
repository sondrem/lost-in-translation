import "./App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./views/Login";
import Translator from "./views/Translator";
import Profile from "./views/Profile";
import NavigationBar from "./components/nav/NavigationBar";
import { Container } from "react-bootstrap";

function App() {
    return (
        <BrowserRouter>
            <NavigationBar />
            <Container>
                <Routes>
                    <Route path="/" element={<Login />} />
                    <Route path="/translator" element={<Translator />} />
                    <Route path="/profile" element={<Profile />} />
                </Routes>
            </Container>
        </BrowserRouter>
    );
}

export default App;
