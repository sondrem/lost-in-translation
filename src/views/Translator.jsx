import { useState } from "react";
import { translationAdd } from "../api/translations";
import TranslatorForm from "../components/translator/TranslatorForm";
import TranslatorOutputSign from "../components/translator/TranslatorOutputSign";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const SIGNS = [
    {
        id: "a",
        image: "images/a.png",
    },
    {
        id: "b",
        image: "images/b.png",
    },
    {
        id: "c",
        image: "images/c.png",
    },
    {
        id: "d",
        image: "images/d.png",
    },
    {
        id: "e",
        image: "images/e.png",
    },
    {
        id: "f",
        image: "images/f.png",
    },
    {
        id: "g",
        image: "images/g.png",
    },
    {
        id: "h",
        image: "images/h.png",
    },
    {
        id: "i",
        image: "images/i.png",
    },
    {
        id: "j",
        image: "images/j.png",
    },
    {
        id: "k",
        image: "images/k.png",
    },
    {
        id: "l",
        image: "images/l.png",
    },
    {
        id: "m",
        image: "images/m.png",
    },
    {
        id: "n",
        image: "images/n.png",
    },
    {
        id: "o",
        image: "images/o.png",
    },
    {
        id: "p",
        image: "images/p.png",
    },
    {
        id: "q",
        image: "images/q.png",
    },
    {
        id: "r",
        image: "images/r.png",
    },
    {
        id: "s",
        image: "images/s.png",
    },
    {
        id: "t",
        image: "images/t.png",
    },
    {
        id: "u",
        image: "images/u.png",
    },
    {
        id: "v",
        image: "images/v.png",
    },
    {
        id: "w",
        image: "images/w.png",
    },
    {
        id: "x",
        image: "images/x.png",
    },
    {
        id: "y",
        image: "images/y.png",
    },
    {
        id: "z",
        image: "images/z.png",
    },
    {
        id: " ",
        image: "images/white.png",
    },
];

const Translator = () => {
    const [sign, setSign] = useState([]);
    const { user, setUser } = useUser();

    /**
     * translates the input to sign language
     * @param {string} translatorInput
     */
    const handleTranslateClicked = async (translatorInput) => {
        //get input from user
        const array = translatorInput.toLowerCase().trim().split("");
        const selectedSigns = array.map((symbol, index) => {
            const signSymbol = SIGNS.find((sign) => symbol === sign.id);
            if (signSymbol) {
                return <TranslatorOutputSign key={index} sign={signSymbol} />;
            }
            return "";
        });

        if (selectedSigns.length === 0) {
            setSign([]);
            alert(
                "Please insert some text to translate.\n" +
                    "Please not that special characters will be ignored"
            );
            return;
        }

        setSign(selectedSigns);

        const [error, updatedUser] = await translationAdd(
            user,
            translatorInput
        );

        if (error !== null) {
            return;
        }

        storageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
    };

    return (
        <>
            <section id="input-section">
                <TranslatorForm onTranslate={handleTranslateClicked} />
            </section>
            {sign.length > 0 && <section id="output-section">{sign}</section>}
        </>
    );
};

export default withAuth(Translator);
