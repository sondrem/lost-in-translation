import ProfileActions from "../components/profile/ProfileActions";
import ProfileHeader from "../components/profile/ProfileHeader";
import ProfileTranslationHistory from "../components/profile/ProfileTranslationHistory";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageDelete } from "../utils/storage";

const Profile = () => {
    const { user, setUser } = useUser();

    const logout = () => {
        storageDelete(STORAGE_KEY_USER);
        setUser(null);
    };

    return (
        <>
            <ProfileHeader username={user.username} />
            <ProfileActions logout={logout} />
            <ProfileTranslationHistory translations={user.translations} />
        </>
    );
};

export default withAuth(Profile);
